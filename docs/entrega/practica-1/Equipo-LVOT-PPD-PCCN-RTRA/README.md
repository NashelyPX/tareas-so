# Reporte practica 1. 
Oscar Tadeo Leal Villegas. `312309202`
Clara Nashely Perez Cruz. `311040616`
Diana Patricio Pascual. `312259097`
Rodrigo Alvar Reina Trejo. `312196785`

Para esta practca se utilizo el programa [*wget2*](https://ftp.gnu.org/pub/gnu/wget/), en especifico se utilizo para esta practica la versión 2.0.0.
# Descripcion del Makefile:

Reglas definidas dentro del archivo Makefile
- `descarga_wet:` Regla para descargar el programa wget2 de la liga https://ftp.gnu.org/pub/gnu/wget/
- `actualiza_autoconf:` Regla para actualizar a la versión 2.71
- `actualiza_automake:`Regla para actualizar a la versión 1.16.4
Los anteriores comandos se mandan a ejecutar para evitar problemas con las MACROSS de la instrucción ./config
- `reload`: Regla utilizado para poder actualizar la terminal y pueda encontrar la nuevas variables actualizadas del entorno
- `configura`: Regla para correr autoconf para generar el script de configuracion a partir de configure.ac o de configure.in
- `configura_p`: Regla genera un archivo configure.help que es el manual de todas la bibliotecas que se pueden agregar u omitir (./configure)
- `configura_prefix:` Regla para:
1.- ./configure es un script que va hacer los cambios en al archivo Makefile y sus argumento le van proporcionar las bibliotecas que utilize
2.- El argumento prefix le indica la ruta donde se va a realizar la instalación.
- `make:` Regla Ejecuta todo el contenido del Makefile de la carpeta wget2.0.0
- `dependencias:` Regla para instalar todas las dependencias necesarias qeu llegue utilizar el programa wget2
- `instalar:` Regla que instala los binarios y paginas de man en la carpeta ${PREFIX}
- `examinar:` Regla para mostrar en terminal la ruta de instalacion y su contenido,
ls -A enlista su contenido
file realiza pruebas para determinar el tipo y  formato de un archivo 
ldd enlista todas la bibliotecas requeridas. 
- `modificar_PATH:`Regla Agregar un linea de configuracion al shell para que el programa compilado se utulzado sin especificar la ruta.

- `verfica:` Regla varifica la instalacion se haya realizado de manera exitosa.
tree: imprime en pantalla los archivos y su contenido en formato de arbol
which wget2: Retorna donde se encuentra el ejecutable de wget2 en la variables de entorno (PATH).

- `regla:` Regla para ejecutar el comando que ejecuta la prueba contenida en *./script.sh*

## asciinema:
[![asciicast](https://asciinema.org/a/l39zwANCqGIZ0Nz0qU5YeR5K2.png)](https://asciinema.org/a/l39zwANCqGIZ0Nz0qU5YeR5K2)



